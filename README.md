# Blockoland
Web site building blocks.

## blockoland-scss
Collection of [SCSS](http://sass-lang.com/) files.

The documentation format can be used with [Hologram](http://trulia.github.io/hologram/), to automatically generate a styleguide.

Check [STYLE.md](STYLE.md) for coding conventions.

## Directories

### reset
CSS Reset.
normalize.scss is based on [normalize.css](https://necolas.github.io/normalize.css/).
reset.scss is based on [reset.css](http://meyerweb.com/eric/tools/css/reset/).
Choose any one you like or write your own!

### settings
Common variables, used throughout the project.

### helpers
Common helper mixins and extends.

### elements
HTML level elements.

### components
Basic building blocks.

### modules
Complex building blocks.

### pages
Page templates.

### License
Licensed under the [MIT license](LICENSE).
